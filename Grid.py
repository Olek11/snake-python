import pygame

def drawGrid(w, rows, surface):
    sizeBtwn = w // rows        #distance between the lines

    x = 0               #keeps track of current x and y
    y = 0
    for l in range(rows):  #Drawing one horizontal and one vertical line each loop
        x = x + sizeBtwn
        y = y + sizeBtwn

        pygame.draw.line(surface, (255, 255, 255), (x, 0), (x, w))
        pygame.draw.line(surface, (255, 255, 255), (0, y), (w, y))

