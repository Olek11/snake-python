import pygame, sys
from Cube import cube

class snake(object):
    body = []
    turns = {}

    def __init__(self, color, pos):
        self.color = color
        self.head = cube(pos)        #making head to be the front of the snake
        self.body.append(self.head)   #adding head to body list

        #these will represent the direction is snake moving
        self.dirnx = 0
        self.dirny = 1

    def move(self):
        for event in pygame.event.get():      #Checking if user use x or escape to exit game
            if event.type == pygame.QUIT:
                sys.exit(0)
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                sys.exit(0)

            keys = pygame.key.get_pressed()    #taking information about key pressed
            for key in keys:    #moving snake by keys
                if keys[pygame.K_LEFT] or keys[pygame.K_a]:
                    self.dirnx = -1
                    self.dirny = 0
                    self.turns[self.head.pos[:]] = [self.dirnx,self.dirny]

                elif keys[pygame.K_RIGHT] or keys[pygame.K_d]:
                    self.dirnx = 1
                    self.dirny = 0
                    self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]

                elif keys[pygame.K_UP] or keys[pygame.K_w]:
                    self.dirnx = 0
                    self.dirny = -1
                    self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]
                elif keys[pygame.K_DOWN] or keys[pygame.K_s]:
                    self.dirnx = 0
                    self.dirny = 1
                    self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]

        for i, c in enumerate(self.body):  #loop through evry cube in body
            p = c.pos[:]        #this stores the cube position in grid
            if p in self.turns:    #if cube position is one where we turned
                turn = self.turns[p]  #get the direction we should turn
                c.move(turn[0], turn[1])    #move cube in that direction
                if i == len(self.body) - 1: #it removes the turn from dict when last cube turned
                    self.turns.pop(p)

            else:    #moving snake to the opposite site after reaching the edge od the screen
                if c.dirnx == -1 and c.pos[0] <= 0:
                    c.pos = (c.rows-1, c.pos[1])
                elif c.dirnx == 1 and c.pos[0] >= c.rows-1:
                    c.pos = (0, c.pos[1])
                elif c.dirny == 1 and c.pos[1] >= c.rows-1:
                    c.pos = (c.pos[0], 0)
                elif c.dirny == -1 and c.pos[1] <= 0:
                    c.pos = (c.pos[0], c.rows-1)
                else:
                     c.move(c.dirnx, c.dirny)   #otherwise move in current direction


    def reset(self, pos):
        self.head = cube(pos)
        self.body = []
        self.body.append(self.head)
        self.turns = {}
        self.dirnx = 0
        self.dirny = 1


    def addCube(self):  #checking where add the cube by checking moving direction
        tail = self.body[-1]
        dx, dy = tail.dirnx, tail.dirny

        if dx == 1 and dy == 0:
            self.body.append(cube((tail.pos[0] - 1, tail.pos[1])))
        elif dx == -1 and dy == 0:
            self.body.append(cube((tail.pos[0] + 1, tail.pos[1])))
        elif dx == 0 and dy == 1:
            self.body.append(cube((tail.pos[0], tail.pos[1] - 1)))
        elif dx == 0 and dy == -1:
            self.body.append(cube((tail.pos[0], tail.pos[1] + 1)))

        #setting cubes direction to the direction of the snake
        self.body[-1].dirnx = dx
        self.body[-1].dirny = dy

    def draw(self, surface):
        for i, c in enumerate(self.body):
            if i == 0:   #for drawing eyes in first cube
                c.draw(surface, True) #adding trues as an argument telling to draw eyes
            else:
                c.draw(surface)    #otherwise it draws normal cube
