import pygame
import random

def randomSnack(rows, item):
    positions = item.body   #get all the positions of cubes in our snake

    while True:  #generating random positions
        x = random.randrange(rows)
        y = random.randrange(rows)
        if len(list(filter(lambda z: z.pos == (x, y), positions))) > 0:  #checking if generated position isn't occupied by snake
            continue
        else:
            break

    return (x, y)
