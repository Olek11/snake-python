import pygame, sys

from Cube import cube
from Snake import snake
from Grid import drawGrid
from Snack import randomSnack
from Message import message_box



def redrawWindow(surface):
    global rows, width, s, snack
    surface.fill((0, 0, 0))      #fills the screen with black
    s.draw(surface)
    snack.draw(surface)
    drawGrid(width, rows, surface)     #draws a grid lines
    pygame.display.update()    #it updates the screen


def main():
    global width, rows, s, snack
    width = 500      #Width of screen
    rows = 20       #Amount of rows
    win = pygame.display.set_mode((width, width))   #Creates screen object
    s = snake((255, 0, 0), (10, 10))            #Creates snake object
    snack = cube(randomSnack(rows, s), color =(0, 255, 0))
    flag = True

    clock = pygame.time.Clock()    #Creating a clock object

    #STARTING MAIN LOOP
    while flag:
        pygame.time.delay(50)       #This delay the game
        clock.tick(10)          #ensure that games run at 10 fps
        s.move()
        if s.body[0].pos == snack.pos:   #checks if the head collides with snack
            s.addCube()    #adds a new cube to the snake
            snack = cube(randomSnack(rows, s), color=(0, 255, 0))     #creates a new snake object

        for x in range(len(s.body)):
            if s.body[x].pos in list(map(lambda z: z.pos, s.body[x + 1:])):   #this will check if any of the positions
                print('Score: ', len(s.body))                                       #in our body overlap
                message_box('You Lost!', 'Play again...')
                s.reset((10, 10))
                break

        redrawWindow(win)    #This will refresh our screen


main()